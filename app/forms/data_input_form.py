from marshmallow import Schema, fields, validate


class DataInputSchema(Schema):
    people_cnt = fields.Int(required=True, validate=validate.Range(min=2))
    date_range = fields.Int(required=True, validate=validate.Range(min=1))
    year_days = fields.Int(validate=validate.OneOf([365, 366]))
