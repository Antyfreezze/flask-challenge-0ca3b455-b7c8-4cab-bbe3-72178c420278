from logging import getLogger
from flask import render_template, flash, make_response, request
from flask_restful import Resource
from marshmallow import ValidationError

from app.forms.data_input_form import DataInputSchema
from app.domain.probability_counter import count_probability


logger = getLogger()


class ProbabilityResource(Resource):

    def get(self):
        return make_response(render_template("data_input.html"))

    def post(self):
        try:
            data = DataInputSchema().load(request.form)
        except ValidationError as e:
            logger.error(e)
            flash("Check your inputs. "
                  "Values have to be: year = 365 or 366, "
                  "people number at least 2, "
                  "range value one and more.")
            return make_response(render_template("data_input.html"))
        result = {
            "probability": count_probability(**data)
        }
        result.update(data)
        return make_response(
            render_template("prob_counter.html", result=result)
            )
