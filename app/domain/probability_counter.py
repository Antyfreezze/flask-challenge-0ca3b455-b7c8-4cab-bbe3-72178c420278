from math import pow, factorial


def count_probability(people_cnt: int, year_days: int, date_range: int):
    numerator = year_days - people_cnt * date_range - 1
    numerator = factorial(numerator)

    denominator = year_days - people_cnt * (date_range + 1)
    denominator = factorial(denominator)
    denominator = int(pow(year_days, (people_cnt - 1))) * denominator

    probability = 1 - numerator/denominator
    return probability
