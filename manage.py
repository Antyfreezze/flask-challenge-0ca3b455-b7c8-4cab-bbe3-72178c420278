import click

from app import create_app
from app.domain.probability_counter import count_probability
from app.forms.data_input_form import DataInputSchema


@click.group()
def cli():
    pass


@click.command()
def runserver():
    app = create_app()
    app.run()


@click.command()
@click.option("-y", "--year-days", default=365, type=int, show_default=True)
@click.option("-d", "--date_range", required=True, type=int)
@click.option("-p", "--people_cnt", required=True, type=int)
def run_as_cli(**kwargs):
    DataInputSchema().load(kwargs)
    print(count_probability(**kwargs))


cli.add_command(runserver)
cli.add_command(run_as_cli)


if __name__ == "__main__":
    cli()
