Project setup:
    Prerequisites:
        Installed python 3.6
        Installed all python dev tools
        Installed pipenv
    Setup:
        Use `$ pipenv install` to setup virtual environment
        To activate pipenv environment run `$ pipenv shell`

Run project:
    Run as flask service:
        Use `$ python manage.py runserver` from project root directory
        Check url `http://localhost:5000/api/smoke`
        If 'smoke' is OK check 'http://localhost:5000/api/probability_counter'
    Run as cli:
        Use `$ python manage.py run-as-cli --help` to see what parameters are expected
        Basic usage example:
            `$ python manage.py run-as-cli -y 365 -d 7 -p 7`
