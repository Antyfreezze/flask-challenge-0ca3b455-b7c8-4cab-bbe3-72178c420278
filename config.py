""" Config file for Flask app """
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class DevelopmentConfig:
    HOST = "localhost"
    PORT = "5000"
    DEBUG = True
    SECRET_KEY = "some_really_secret_key"
    FLASK_ENV = "development"
